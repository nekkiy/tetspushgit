package ru.inordic.arj.gradle;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class FutureExample {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        FutureExample f = new FutureExample();

        System.out.println(Thread.currentThread().getId()+" Основной поток шаг 0");
        Future<String> future = f.killSpace("asdasd  asda asad asda asd     ");

        System.out.println(Thread.currentThread().getId()+" Основной поток шаг 1");
        String result = future.get();

        System.out.println(Thread.currentThread().getId()+" Основной поток шаг 2");

    }

    public Future<String> killSpace(String str) {
        System.out.println(Thread.currentThread().getId()+" Основной поток шаг 0.1");
        CompletableFuture<String> future = new CompletableFuture<>();
        System.out.println(Thread.currentThread().getId()+" Основной поток шаг 0.2");
        new Thread(new Runnable() {
            @Override
            public void run() {

                System.out.println(Thread.currentThread().getId()+" Прикладной поток шаг 0");
                String result = str.replaceAll(" ", "");

                System.out.println(Thread.currentThread().getId()+" Прикладной поток шаг 1");
                future.complete(result);

                System.out.println(Thread.currentThread().getId()+" Прикладной поток шаг 2");
            }
        }).start();

        System.out.println(Thread.currentThread().getId()+" Основной поток шаг 0.3");
        return future;
    }

}
