package ru.inordic.arj.gradle;

import java.util.concurrent.atomic.AtomicBoolean;

public class ThreadExample {
    public static void main(String[] args) throws InterruptedException {
        AtomicBoolean isStop = new AtomicBoolean(false);
//        boolean flag = false;
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 6; i++) {
                    if (isStop.get()) {
                        return;
                    }
                    try {
                        Thread.sleep(2000);
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                    System.out.println("Привет, мир");
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
        Thread.sleep(4000);
        isStop.set(true);

        for (int i = 0; i < 6; i++) {
            try {
                Thread.sleep(1000);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            System.out.println("После start'ие");
        }
        thread.join();
    }
}
