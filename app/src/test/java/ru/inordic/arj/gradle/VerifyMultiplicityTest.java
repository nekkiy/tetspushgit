package ru.inordic.arj.gradle;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class VerifyMultiplicityTest {

    @Test
    void verifyTwo() {
        VerifyMultiplicity verifier = new VerifyMultiplicity();

        Assertions.assertTrue(verifier.verifyTwo(new int[]{9, 0}));

        Assertions.assertFalse(verifier.verifyTwo(new int[]{9, 1}));
    }

    @Test
    void verifyThree() {
        VerifyMultiplicity verifier = new VerifyMultiplicity();
        boolean result = verifier.verifyTwo(new int[]{9, 0});
        Assertions.assertEquals(true, result);
    }

    @Test
    void verifyFour() {
        VerifyMultiplicity verifier = new VerifyMultiplicity();
        boolean result = verifier.verifyTwo(new int[]{9, 0});
        Assertions.assertEquals(false, result);
    }

    @Test
    void verifyFive() {
        VerifyMultiplicity verifier = new VerifyMultiplicity();
        boolean result = verifier.verifyTwo(new int[]{9, 0});
        Assertions.assertEquals(true, result);
    }

    @Test
    void verifyNine() {
        VerifyMultiplicity verifier = new VerifyMultiplicity();
        boolean result = verifier.verifyTwo(new int[]{9, 0});
        Assertions.assertEquals(true, result);
    }

    @Test
    void verifyEleven() {
        VerifyMultiplicity verifier = new VerifyMultiplicity();
        boolean result = verifier.verifyTwo(new int[]{9, 0});
        Assertions.assertEquals(false, result);
    }
}
